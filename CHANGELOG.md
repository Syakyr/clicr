
<a id='changelog-0.0.1'></a>
# 0.0.1 — 2022-10-27

## Added

- Started project
- Set up the scaffolding of documentation page with Gitlab CI and MkDocs
- Used mkdocs and mkdocs-material as documentation engine
- Added `clicr` command
- Added README with installation instructions (dev)
- Added `init` sub-command - Creates a new CLI source code from template
