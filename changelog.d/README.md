# Managed by Scriv

This directory will hold the changelog entries managed by [Scriv].
Do NOT remove this directory.

[Scriv]: https://github.com/nedbat/scriv
