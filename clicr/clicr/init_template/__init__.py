import importlib.metadata as md

__version__ = md.version(__name__)
