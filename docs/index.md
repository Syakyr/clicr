# Welcome to the `clicr` documentation!

`clicr` (pronounced as *clicker*) is the CLI Creator aims to be the 
template used to make effective CLI apps using Python and its relevant 
packages.

## Why another template?

There're existing templates when it comes to creating CLI apps with 
Python, but they are not simple enough for a normal Python programmer
to pick up and use directly. Hopefully this would alleviate that 
problem with yet another template... yeah, that [XKCD] meme.

[XKCD]: https://xkcd.com/927/

## Getting Started

Install Poetry:

```python
# Pip install if using venv
pip install poetry

# Conda create new env and install
conda create -n clicr poetry
conda activate clicr

# Conda install in existing environment
conda install poetry
```

Then install the dependencies in Poetry:
```python
poetry install
```

You can now use the `clicr` in the CLI.
Check `clicr --help` for more information.

To run the documentation, run `mkdocs server` and check out the site at 
`localhost:8000`.

## Managing Changelog

This repository's changelog is managed by [Scriv]

[Scriv]: https://github.com/nedbat/scriv

## License

© Copyright 2022 Syakyr Surani.
This project is licensed under [Apache Software License 2.0](LICENSE).

