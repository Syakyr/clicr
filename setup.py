from setuptools import setup, find_packages

setup(
    name="clicr",
    version="0.0.3",
    description="Make effective CLI apps using Python",
    author="Syakyr Surani",
    author_email="gitlab@syakyr.com",
    url="https://syakyr.gitlab.io/clicr",
    license="Apache-2.0",
    classifiers=[
        "Development Status :: 1 - Planning",
        "Topic :: Software Development :: Build Tools",
        "Topic :: Software Development :: Libraries :: Python Modules",
    ],
    packages=find_packages(),
    install_requires=[
        "typer[all]>=0.9.0",
    ],
    extras_require={
        "dev": [
            "pytest>=5.2",
            "scriv[toml]>=0.17.0",
            "mkdocs>=1.3.0",
            "mkdocs-material>=8.3.3",
            "twine"
        ]
    },
    entry_points={
        "console_scripts": [
            "clicr = clicr.clicr.main:app",
        ]
    },
)